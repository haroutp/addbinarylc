﻿using System;
using System.Collections.Generic;
using System.Text;


namespace AddBinary
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(AddBinary("11", "1"));
        }

        public static string AddBinary(string a, string b) {
            StringBuilder sb = new StringBuilder();

            int i = a.Length - 1;
            int j = b.Length - 1;
            int carry = 0;

            while (i >= 0 || j >= 0)
            {
                int sum = carry;
                if(i >= 0){
                    // '0' converting characters into ints easily
                    // '1' - '0' yields a 1 ---- '0' - '0' yields a 0
                    sum += (a[i--] - '0');
                }

                if(j >= 0){
                    sum += (b[j--] - '0');
                }
                sb.Insert(0, sum % 2);
                carry = sum / 2;
            }

            if(carry > 0){
                sb.Insert(0, 1);
            }

            return Convert.ToString(sb);
            
        }
    }
}
